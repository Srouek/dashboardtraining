import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';

@Component({
	selector: 'app-widget-area',
	templateUrl: './area.component.html',
	styleUrls: [ './area.component.scss' ]
})
export class AreaComponent implements OnInit {
	chartOptions: {};
	@Input() data = [];

	Highcharts = Highcharts;

	constructor() {}

	ngOnInit(): void {
		this.chartOptions = {
			chart: {
				type: 'area'
			},
			title: {
				text: 'Demo Chart from Highcharts'
			},
			subtitle: {
				text: 'https://www.highcharts.com'
			},
			tooltip: {
				split: true,
				valueSuffix: ' millions'
			},
			credits: {
				enabled: false
			},
			exporting: {
				enabled: true
			},
			series: this.data
		};

		HC_exporting(Highcharts);

		/** Trick to keep the chart responsive  */
		setTimeout(() => {
			window.dispatchEvent(new Event('resize'));
		}, 300);
	}
}
